package org.example.Lab4;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class LabFourTest {
    private static final String baseUrl = "https://ca1c15e2-01ef-421a-a63f-04741aa26989.mock.pstmn.io";

    private static final String GET = "/ownerName",
                                GET_UNSUCCESS = GET + "/unsuccess",
                                POST = "/createSomething",
                                PUT = "/updateMe",
                                DELETE = "/deleteWorld";

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void verifySuccessfulGetAction() {
        String name = "Ihor Velychko";
        given()
                .get(GET)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .body("name", equalTo(name));
    }

    @Test(dependsOnMethods = "verifySuccessfulGetAction")
    public void verifyUnsuccessfulGetAction() {
        String exception = "I won't say my name!";
        given()
                .get(GET_UNSUCCESS)
                .then()
                .statusCode(HttpStatus.SC_FORBIDDEN)
                .and()
                .body("exception", equalTo(exception));
    }

    @Test(dependsOnMethods = "verifyUnsuccessfulGetAction")
    public void verifySuccessfulPostAction() {
        String result = "'Nothing' was created";
        given()
                .queryParam("permission", "yes")
                .post(POST)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .body("result", equalTo(result));
    }

    @Test(dependsOnMethods = "verifySuccessfulPostAction")
    public void verifyUnsuccessfulPostAction() {
        String result = "You don't have permission to create Something";
        given()
                .post(POST)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .and()
                .body("result", equalTo(result));
    }

    @Test(dependsOnMethods = "verifyUnsuccessfulPostAction")
    public void verifyPutAction() {
        Map<String, String> body = new HashMap<>();
        body.put("name", "");
        body.put("surname", "");

        given()
                .body(body)
                .put(PUT)
                .then()
                .statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test(dependsOnMethods = "verifyPutAction")
    public void verifyDeleteAction() {
        String world = "0";

        given()
                .header("SessionID", "123456789")
                .delete(DELETE)
                .then()
                .statusCode(HttpStatus.SC_GONE)
                .and()
                .body("world", equalTo(world));
    }
}
