package org.example.Lab3;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.*;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class PetTest {
    private static final String baseUrl = "https://petstore.swagger.io/v2";

    private static final String USER = "/user",
            USER_USERNAME = USER + "/{username}",
            USER_LOGIN = USER + "/login",
            USER_LOGOUT = USER + "/logout",
            PET = "/pet",
            PET_PET_ID = PET + "/{petId}";

    private static final int MY_GROUP = 1222034;
    private int petId;
    private int categoryId;
    private String categoryName;
    Map<String, Object> category = new HashMap<>();
    private String petName;
    private String photoUrl;
    private List<String> photoUrls;
    private int tagId;
    private String tagName;
    private Map<String, Object> tag = new HashMap<>();
    private List<Map<String, Object>> tags = new ArrayList<>();
    private String petStatus;

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void verifyLoginAction() {
        Map<String, ?> body = Map.of(
                "username", "IhorVelychko",
                "password", "122-20-3.4"
        );
        Response response = given().body(body)
                .get(USER_LOGIN);
        response.then()
                .statusCode(HttpStatus.SC_OK);
        RestAssured.requestSpecification
                .sessionId(response.jsonPath()
                        .get("message")
                        .toString()
                        .replaceAll("[^0-9]", ""));
    }

    @Test(dependsOnMethods = "verifyLoginAction")
    public void verifyCreateAction() {
        petId = MY_GROUP;
        categoryId = MY_GROUP;
        categoryName = "Dog";
        petName = Faker.instance().animal().name();
        photoUrl = "photo_url";
        tagId = MY_GROUP;
        tagName = "Tag";
        petStatus = "available";

        category.put("id", categoryId);
        category.put("name", categoryName);

        photoUrls = Collections.singletonList(photoUrl);

        tag.put("id", tagId);
        tag.put("name", tagName);

        tags.add(tag);

        Map<String, Object> body = new HashMap<>();
        body.put("id", petId);
        body.put("category", category);
        body.put("name", petName);
        body.put("photoUrls", photoUrls);
        body.put("tags", tags);
        body.put("status", petStatus);
        given().body(body)
                .post(PET)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "verifyCreateAction")
    public void verifyGetCreatedAction() {
        given().pathParams("petId", petId)
                .get(PET_PET_ID)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .body("id", equalTo(petId))
                .body("category", equalTo(category))
                .body("photoUrls", equalTo(photoUrls))
                .body("tags", equalTo(tags))
                .body("status", equalTo(petStatus))
                .body("name", equalTo(petName));
    }

    @Test(dependsOnMethods = "verifyGetCreatedAction")
    public void verifyUpdateAction() {
        petName = Faker.instance().animal().name();
        petStatus = "not available";

        Map<String, Object> body = new HashMap<>();
        body.put("id", petId);
        body.put("category", category);
        body.put("name", petName);
        body.put("photoUrls", photoUrls);
        body.put("tags", tags);
        body.put("status", petStatus);
        given().body(body)
                .put(PET)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "verifyUpdateAction")
    public void verifyGetUpdatedAction() {
        given().pathParams("petId", petId)
                .get(PET_PET_ID)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .body("id", equalTo(petId))
                .body("category", equalTo(category))
                .body("photoUrls", equalTo(photoUrls))
                .body("tags", equalTo(tags))
                .body("status", equalTo(petStatus))
                .body("name", equalTo(petName));
    }

    @Test(dependsOnMethods = "verifyGetUpdatedAction")
    public void verifyDeleteAction() {
        given().pathParams("petId", petId)
                .delete(PET_PET_ID)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "verifyLoginAction", priority = 1)
    public void verifyLogoutAction() {
        given().get(USER_LOGOUT)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }
}
