package org.example.Lab2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.security.Key;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class RozetkaTest {
    WebDriver edgeDriver;

    private static final String baseUrl = "https://rozetka.com.ua/ua/";

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        // Run driver
        WebDriverManager.edgedriver().setup();
        EdgeOptions edgeOptions = new EdgeOptions();
        // Set fullscreen
        edgeOptions.addArguments("--start-fullscreen");
        // setup wait for loading elements
        edgeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        this.edgeDriver = new EdgeDriver(edgeOptions);
    }

    @BeforeMethod
    public void preconditions() {
        // open main page
        edgeDriver.get(baseUrl);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        edgeDriver.quit();
    }

    @Test
    public void testProductSearch() {
        WebElement searchField = edgeDriver.findElement(By.tagName("input"));
        Assert.assertNotNull(searchField);

        String inputValue = "смартфон";
        searchField.click();
        searchField.sendKeys(inputValue);
        Assert.assertEquals(searchField.getAttribute("value"), inputValue);
        searchField.sendKeys(Keys.ENTER);
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        By compoundLocator = By.cssSelector("rz-button-product-page a");
        WebElement itemLink = edgeDriver.findElement(compoundLocator);
        Assert.assertNotNull(itemLink);

        String searchUrl = edgeDriver.getCurrentUrl();
        itemLink.click();
        try {
            WebDriverWait waitUrlChange = new WebDriverWait(edgeDriver, Duration.ofSeconds(5));
            waitUrlChange.until(ExpectedConditions.not(ExpectedConditions.urlToBe(searchUrl)));
        } catch (TimeoutException e) {
            System.out.println("URL hasn't changed for a long time.");
        }
        Assert.assertNotEquals(edgeDriver.getCurrentUrl(), searchUrl);
    }
}
