package org.example.Lab2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class FirstLabTest
{
    WebDriver edgeDriver;

    private static final String baseUrl = "https://www.nmu.org.ua/ua/";

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        // Run driver
        WebDriverManager.edgedriver().setup();
        EdgeOptions edgeOptions = new EdgeOptions();
        // Set fullscreen
        edgeOptions.addArguments("--start-fullscreen");
        // setup wait for loading elements
        edgeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        this.edgeDriver = new EdgeDriver(edgeOptions);
    }

    @BeforeMethod
    public void preconditions() {
        // open main page
        edgeDriver.get(baseUrl);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        edgeDriver.quit();
    }

    @Test
    public void testHeaderExists() {
        // find element by id
        WebElement header = edgeDriver.findElement(By.id("heder"));
        // verification
        Assert.assertNotNull(header);
    }

    @Test
    public void testClickOnForStudent() {
        // find element by xpath
        WebElement forStudentButton = edgeDriver.findElement(By.xpath("/html/body/center/div[4]/div/div[1]/ul/li[4]/a"));
        // verification
        Assert.assertNotNull(forStudentButton);
        forStudentButton.click();
        // verification page changed
        Assert.assertNotEquals(edgeDriver.getCurrentUrl(), baseUrl);
    }

    @Test
    public void testSearchFieldOnForStudentPage() {
        String studentPageUrl = "content/students/";
        String searchPageUrl = "search/";
        edgeDriver.get(baseUrl + studentPageUrl);
        // find element by tag name
        WebElement searchField = edgeDriver.findElement(By.tagName("input"));
        // verification
        Assert.assertNotNull(searchField);
        searchField.click();
        Assert.assertEquals(edgeDriver.getCurrentUrl(), baseUrl + searchPageUrl);
        WebElement actualSearchField = edgeDriver.findElement(By.id("gsc-i-id1"));
        // different params of searchField
        System.out.println( String.format("Name attribute: %s", actualSearchField.getAttribute("name")) +
                String.format("\nID attribute: %s", actualSearchField.getAttribute("id")) +
                String.format("\nType attribute: %s", actualSearchField.getAttribute("type")) +
                String.format("\nName attribute: %s", actualSearchField.getAttribute("name")) +
                String.format("\nValue attribute: %s", actualSearchField.getAttribute("value")) +
                String.format("\nPosition: (%d;%d)", actualSearchField.getLocation().x, actualSearchField.getLocation().y) +
                String.format("\nSize: %dx%d", actualSearchField.getSize().height, actualSearchField.getSize().width)
        );
        // input value
        String inputValue = "I need info";
        actualSearchField.click();
        actualSearchField.sendKeys(inputValue);
        // verification text
        Assert.assertEquals(actualSearchField.getAttribute("value"), inputValue);
        // click enter
        actualSearchField.sendKeys(Keys.ENTER);
        // verification page changed
        WebElement searchResultWrapper = edgeDriver.findElement(By.className("gsc-wrapper"));
        Assert.assertNotNull(searchResultWrapper);
    }

    @Test
    public void testSlider() {
        // find element by class name
        WebElement nextButton = edgeDriver.findElement(By.className("next"));
        // find element by css selector
        WebElement nextButtonByCss = edgeDriver.findElement(By.cssSelector("a.next"));
        // verification equality
        Assert.assertEquals(nextButtonByCss, nextButton);
        WebElement previousButton = edgeDriver.findElement(By.className("prev"));
        while (!nextButton.getAttribute("class").contains("disabled")) {
            nextButton.click();
        }
        Assert.assertTrue(nextButton.getAttribute("class").contains("disabled"));
        Assert.assertFalse(previousButton.getAttribute("class").contains("disabled"));

        while (!previousButton.getAttribute("class").contains("disabled")) {
            previousButton.click();
        }
        Assert.assertTrue(previousButton.getAttribute("class").contains("disabled"));
        Assert.assertFalse(nextButton.getAttribute("class").contains("disabled"));
    }
}
